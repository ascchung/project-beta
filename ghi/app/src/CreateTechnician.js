import React, { useState } from "react";

const CreateTechnician = () => {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const handleChange = (event) => {
    const name = event.target.name;
    const input = event.target.value;

    setFormData({
      ...formData,
      [name]: input,
    });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8080/api/technicians/";

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-type": "application/json",
      },
    };

    const req = await fetch(url, fetchOptions);

    if (req.ok) {
      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
      alert("Technician created successfully!");
    }
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1>Create a Technician</h1>
        <div className="mb-3">
          <label htmlFor="color" className="form-label">
            First Name
          </label>
          <input
            type="text"
            className="form-control"
            placeholder="First Name"
            onChange={handleChange}
            value={formData.first_name}
            name="first_name"
          />
          <label htmlFor="year" className="form-label">
            Last Name
          </label>
          <input
            type="text"
            className="form-control"
            placeholder="Last Name"
            onChange={handleChange}
            value={formData.last_name}
            name="last_name"
          />
          <label htmlFor="vin" className="form-label">
            Employee ID
          </label>
          {formData.employee_id.length > 10 ? (
            <>
              <input
                type="text"
                className="form-control"
                placeholder="Employee ID"
                onChange={handleChange}
                value={formData.employee_id}
                name="employee_id"
              />
              <p className="text-danger">
                Employee ID cannot exceed 10 characters
              </p>
            </>
          ) : (
            <input
              type="text"
              className="form-control"
              placeholder="Employee ID"
              onChange={handleChange}
              value={formData.employee_id}
              name="employee_id"
            />
          )}
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </>
  );
};

export default CreateTechnician;
