import React, { useEffect, useState } from 'react';

export default function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [filtered, setFiltered] = useState('');

    const handleSalespersonChange = (e) => {
        setFiltered(e.target.value);
    };

    const fetchData = async () => {
        const salesUrl = 'http://localhost:8090/api/sales';
        const salesResponse = await fetch(salesUrl);

        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        };

        const salespeopleUrl = 'http://localhost:8090/api/salespeople';
        const salespeopleResponse = await fetch(salespeopleUrl);

        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
        };
    };

    const filteredSales = sales.filter((sale) => {
        const salespersonFullName = `${sale.salesperson.first_name} ${sale.salesperson.last_name}`.toLowerCase()
        return salespersonFullName.includes(filtered.toLowerCase());
    });

    
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Salesperson History</h1>
            <select
                value={filtered}
                onChange={handleSalespersonChange}
                name="salesperson"
                label="Salesperson"
                className="form-select">
                    <option value="">Select a salesperson</option>
                {salespeople.map(salesperson => (
                    <option
                        key={salesperson.id}
                        value={`${salesperson.first_name} ${salesperson.last_name}`}>
                            {salesperson.first_name} {salesperson.last_name}
                    </option>
                ))}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales && filteredSales.map((sale) => {
                        return (
                        <tr key={ sale.id }>
                            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
};
