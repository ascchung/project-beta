import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg bg-dark">
      <div className="container-fluid">
        <a className="navbar-brand text-light" href="/">
          AutoVista
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-light"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-primary"
                    to="/manufacturers"
                  >
                    Manufacturers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-primary"
                    to="/manufacturers/create"
                  >
                    Create a Manufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link text-primary" to="/models">
                    Models
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-primary"
                    to="/models/create"
                  >
                    Create a Model
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link text-primary" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-primary"
                    to="/automobiles/create"
                  >
                    Create an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-light"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/create">
                    Create Salesperson
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople">
                    Salespeople
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customer/create">
                    Add a Customer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers">
                    Customers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/create">
                    Add a Sale
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/">
                    Sales
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salesperson/history">
                    Salesperson History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-light"
                href="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians">
                    Technicians
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/create">
                    Create Technician
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments">
                    Appointments
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/create">
                    Create Appointment
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/history">
                    Appointment History
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
