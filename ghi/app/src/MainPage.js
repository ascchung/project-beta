import React from 'react';

function MainPage() {
  return (
    <main className="bg-light min-vh-100 d-flex flex-column align-items-center justify-content-center">
      <div className="container text-center">
        <div className="banner p-4 mb-4 bg-light">
          <h1 className="display-5 fw-bold">AutoVista</h1>
            <p className="lead mb-4">
              The premiere solution for automobile dealership management!
            </p>
          <img src="/mainbanner.jpeg" className="img-fluid mb-4" alt="Main banner image" style={{ maxHeight: '500px', width: '100%', objectFit: 'cover' }} />
        </div>
      </div>
    </main>
  );
}

export default MainPage;
