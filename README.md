# AutoVista

AutoVista is an application that allows a user to manage cars at a dealership. The application is able to manage inventory, automobile sales, and automobile services.

Team:

- Person 1 - Andrew Chung - Sales
- Person 2 - Joseph Lang - Services

## How to get started

1. install git, docker, and node on your device.
2. Fork the repository
3. Clone the repository via http in your terminal in your desired directory.
4. run
   `docker volume create beta-data`
   `docker-compose build`
   `docker-compose up`
5. Check to make sure all containers are running and there are no errors in the logs.
6. You are setup!

## Diagram

Please click [img](/images/carcar_diagram.png)

## Design

The application consists of 3 microservices that interact with each other: -**Inventory** -**Sales** -**Services**

## API Documentation

### URLs and Ports

-To see frontend in browser: http://localhost:3000/
-Inventory: http://localhost:8100/
-Sales: http://localhost:8090/
-Services: http://localhost:8080/

### Manufacturers:

| Action                         | Method | URL                                         |
| ------------------------------ | ------ | ------------------------------------------- |
| List manufacturers             | GET    | http://localhost:8100/api/manufacturers/    |
| Create a manufacturer          | POST   | http://localhost:8100/api/manufacturers/    |
| Get a specific manufacturer    | GET    | http://localhost:8100/api/manufacturers/id/ |
| Update a specific manufacturer | PUT    | http://localhost:8100/api/manufacturers/id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/ |

JSON body to send the data:

Creating and updating a manufacturer requires only the manufacturer's name. Can only list one manufacturer at a time.

```
{
  "name": "Chrysler"
}
```

The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```

The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Vehicle Models

| Action                          | Method | URL                                   |
| ------------------------------- | ------ | ------------------------------------- |
| List vehicle models             | GET    | http://localhost:8100/api/models/     |
| Create a vehicle model          | POST   | http://localhost:8100/api/models/     |
| Get a specific vehicle model    | GET    | http://localhost:8100/api/models/:id/ |
| Update a specific vehicle model | PUT    | http://localhost:8100/api/models/:id/ |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/ |

Creating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information.

```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```

Getting a list of vehicle models returns a list of the detail information with the key "models".

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

## Automobiles

| Action                       | Method | URL                                         |
| ---------------------------- | ------ | ------------------------------------------- |
| List automobiles             | GET    | http://localhost:8100/api/automobiles/      |
| Create an automobile         | POST   | http://localhost:8100/api/automobiles/      |
| Get a specific automobile    | GET    | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT    | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

You can create an automobile with its color, year, VIN, and the id of the vehicle model.

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

To return details of the car, you would use the URL

http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

in order to return the details of the car.

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```

You can update the color, year, and sold status of an automobile.

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```

Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}
```

## Service microservice

The service microservice contains 3 models which are Appointment, AutomobileVO, and Technician. The AutomobileVO allows the this microservice to check if a customer purchased a vehicle from our inventory and mark them as VIP if they did.

### API Endpoints

#### Technician

| Action            | Method | URL                                    |
| ----------------- | ------ | -------------------------------------- |
| List Technicians  | GET    | http://localhost:8080/api/technicians/ |
| Create Technician | POST   | http://localhost:8080/api/technicians/ |

List Technicians Return Value

```

{
	"technicians": [
		{
			"first_name": "Bob",
			"last_name": "Smith",
			"employee_id": "hhjdf3"
		}
  ]
}
```

Create Technician Post Request:

```
{
	"first_name": "Bob",
	"last_name": "Smith",
	"employee_id": "fhdjk89"
}

```

Create Technician Return Value:

```
"Technician created successfully!"
```

#### Appointment

| Action                             | Method | URL                                               |
| ---------------------------------- | ------ | ------------------------------------------------- |
| List Appointments listAppointments | GET    | http://localhost:8080/api/appointments/           |
| Create Technician                  | POST   | http://localhost:8080/api/appointments/           |
| Set Appointment to Finished        | PUT    | http://localhost:8080/api/appointments/id/finish/ |
| Set Appointment to Canceled        | PUT    | http://localhost:8080/api/appointments/id/cancel/ |

List Appointments Return Value:
#listAppointments

```
{
	"appointments": [
		{
			"date_time": "2023-10-25T14:30:59+00:00",
			"reason": "flat tire",
			"status": "finished",
			"vin": "HJK987",
			"customer": "Billy Hill",
			"technician": {
				"first_name": "Bob",
				"last_name": "Smith"
			},
			"id": 33
    }
  ]
 }
```

Create Appointment Post Request:

```
{
	"date": "2023-10-25",
	"time": "14:30",
	"reason": "oil change",
	"vin": "HJhjk89",
	"customer": "John Doe",
	"technician": "hhjdf3"
}
```

Create an Appointment Return Value:

```
"Appointment successfully created!"
```

Set Appointment to Finished Return Value:

```
"Appointment successfully marked as finished!"
```

Set Appointment to Cancelled Return Value:

```
"Appointment cancelled successfully!"
```

## Sales microservice

Backend of sales microservice contains 4 models: Salesperson, Customer, AutomobileVO, and Sale. The Sale model is the one that interacts with the other three models and grabs data from them.

The AutomobileVO is a value object that grabs data on the automobiles in the inventory using the poller. The sales poller refreshes automatically every 60 seconds and polls the inventory so that the sales microservice is updated with its data.

The purpose of intergrating between these two microservices is so that when recording a new sale, this data needs to be sent over to which car is being sold in the inventory microservice.

### Salespeople

| Action               | Method | URL                                        |
| -------------------- | ------ | ------------------------------------------ |
| List salespeople     | GET    | http://localhost:8090/api/salespeople/     |
| Create a salesperson | POST   | http://localhost:8090/api/salespeople/     |
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/:id/ |

JSON body to send the data:

```
{
	"first_name": "Jimmy",
	"last_name": "Bob",
	"employee_id": "1234"
}
```

Return Value after creating a salesperson:

```
{
	"salesperson": {
		"first_name": "Jimmy",
		"last_name": "Bob",
		"employee_id": "1234",
		"id": 1
	}
}
```

Listing all salespeople Return Value:

```
{
	"salespeople": [
		{
			"first_name": "Jimmy",
			"last_name": "Bobby",
			"employee_id": 2468,
			"id": 2
		},
		{
			"first_name": "Ricky",
			"last_name": "Bobby",
			"employee_id": 6542,
			"id": 6
		}
	]
}
```

### Sale

**The "id" is tied to the specific salesperson.**

| Action        | Method | URL                                  |
| ------------- | ------ | ------------------------------------ |
| List sales    | GET    | http://localhost:8090/api/sales/     |
| Create a sale | POST   | http://localhost:8090/api/sales/     |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id/ |

Creating a sale to send in the JSON body:

```
{
  "salesperson": "5",
	"customer":"2",
	"automobile": "1FTPW14V09FA28786",
	"price": "5000"
}
```

The Return Value after creating a sale:

```
{
	"sale": {
		"automobile": {
			"sold": true,
			"vin": "1FTPW14V09FA28786"
		},
		"salesperson": {
			"first_name": "Kim",
			"last_name": "Bob",
			"employee_id": 9842,
			"id": 5
		},
		"customer": {
			"first_name": "Ollie",
			"last_name": "Myers",
			"address": "421 Broadway Avenue",
			"phone_number": "909-900-9009",
			"id": 2
		},
		"price": "5000",
		"id": 2
	}
}
```

Listing all sales Return Value:

```
{
	"sales": [
		{
			"automobile": {
				"sold": true,
				"vin": "1FTPW14V09FA28786"
			},
			"salesperson": {
				"first_name": "Kim",
				"last_name": "Bob",
				"employee_id": 9842,
				"id": 5
			},
			"customer": {
				"first_name": "Ollie",
				"last_name": "Myers",
				"address": "421 Broadway Avenue",
				"phone_number": "909-900-9009",
				"id": 2
			},
			"price": 5000,
			"id": 2
		}
	]
}
```

### Customer

| Action            | Method | URL                                      |
| ----------------- | ------ | ---------------------------------------- |
| List customers    | GET    | http://localhost:8090/api/customers/     |
| Create a customer | POST   | http://localhost:8090/api/customers/     |
| Delete a customer | DELETE | http://localhost:8090/api/customers/:id/ |

Creating a customer to send in the JSON body:

```
{
	"first_name": "Ollie",
	"last_name": "Myers",
	"address": "421 Broadway Avenue",
	"phone_number": "909-900-9009"
}
```

The Return Value after creating customer:

```
{
	"customer": {
		"first_name": "Ollie",
		"last_name": "Myers",
		"address": "421 Broadway Avenue",
		"phone_number": "909-900-9009",
		"id": 2
	}
}
```

Listing the customers Return Value:

```
{
	"customers": [
		{
			"first_name": "Nancy",
			"last_name": "Jones",
			"address": "100 South Miller St.",
			"phone_number": "111-111-1111",
			"id": 1
		},
		{
			"first_name": "Ollie",
			"last_name": "Myers",
			"address": "421 Broadway Avenue",
			"phone_number": "909-900-9009",
			"id": 2
		}
	]
}
```
